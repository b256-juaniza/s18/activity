// console.log("Hello Universe");

// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked

// Functions are mostly created to create complicated tasks to run several lines of code in succession

// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

function printInput() {

	let nickname = prompt("Enter your Nickname: ");
	console.log("Hi, " + nickname);
}

printInput();

//However, for some use cases, this may not be ideal. 
//For other cases, functions can also process data directly passed into it instead of relying only on Global Variables and prompt().

function printName(name) {

	console.log("My name is " + name);

}

// A "parameter" acts as a named variable/container that exists only inside of a function
// It is used to store information that is provided to a function when it is called/invoked.

//"Juana", the information/data provided directly into the function is called an argument.
//Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.
printName("Juana");
printName("John");
printName("Jane");

// Variables can be also be passed as an argument
let sampleVariable = "Joker";
printName(sampleVariable);

//Function arguments cannot be used by a function if there are no parameters provided within the function.

function checkDivisibilityBy8(num) {
	let remainder = num % 8;

	console.log("The remainder of " + num + " divided by 8 is " + remainder);

	let isDividedBy8 = remainder === 0;
	console.log("is " + num + " divisible by 8? ");
	console.log(isDividedBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(99);
checkDivisibilityBy8(101);

function argumentFunction() {
	console.log("This function is passed as an argument before the message was printed.");
}

function invokeFunction(argumentFunction) {
	argumentFunction();
}

invokeFunction(argumentFunction);

console.log(argumentFunction);

// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela");
createFullName("Juan", "Dela", "Cruz", "Smith");

// Variable as arguments
let fName = "Jake";
let mName = "Galing";
let lName = "Sapa";

createFullName(fName, mName, lName);

// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

function returnFullName(firstName, middleName, lastName) {
	return firstName + " " + middleName + " " + lastName;
	console.log("I am here but not seen");
}

let completeName = returnFullName("Yamamoto", "D.", "Takeshi");
console.log(completeName);

function returnAddress(city, country) {
	let fullAddress = city + " " + country;
	return fullAddress;
}

let myAddress = returnAddress("Manila", "Philippines");
console.log(myAddress);

function printPlayerInfo(uname, level, job) {
	console.log(uname);
	console.log(level);
	console.log(job);
}

let user1 = printPlayerInfo("white_knight", 80, "Paladin");
console.log(user1);